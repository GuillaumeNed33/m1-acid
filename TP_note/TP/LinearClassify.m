function [result] = LinearClassify(test, W)
    mat = [ones(1,size(test, 1)) ; test'];
    result = W' * mat < 0;
end
