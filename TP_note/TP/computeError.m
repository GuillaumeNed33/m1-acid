function [SaumonError, BarError ] = computeError(ResSaumon, ResBar )
    nbBarError    = size(ResBar,1) - sum(ResBar);
    sizeBar = size(ResBar,1);
    BarError = nbBarError / size(ResBar,1); %*100 pour le pourcentage

    nbSaumonError = sum(ResSaumon);
    sizeSaumon = size(ResSaumon,1);
    SaumonError = nbSaumonError / size(ResSaumon,1); %*100 pour le pourcentage
end
