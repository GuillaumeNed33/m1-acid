function [result] = BayesienCoutWithMaxPosteriori(test1, mu1, sigma1, mu2, sigma2, tabCout, proba1, proba2)
    CoutSaumonCommeBar = tabCout(1,2); %lambda bs
    CoutBarCommeSaumon = tabCout(2,1); %lambda sb
    
    prob1= normpdf(test1, mu1, sigma1);
    prob2 = normpdf(test1, mu2, sigma2);
    
    RiskSaumon = CoutBarCommeSaumon * prob1;
    RiskBar = CoutSaumonCommeBar * prob2;

    result = (CoutSaumonCommeBar * proba2 * prob2) < (CoutBarCommeSaumon * proba1 * prob1);
endnd