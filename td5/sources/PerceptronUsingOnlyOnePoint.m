function [W] = PerceptronUsingOnlyOnePoint(C1, C2, W)
    epsilon = 0.01; % critere d'arret
    nu = 0.015;
    Y = CalculMatY(C1,C2);
    Ym = ChercherMauvaisPlacement(Y, W);
    index = randi(size(Ym,2));
    RandomPoint = [Ym(2,index) Ym(3,index)];
    wCurrent = W + nu * sum(RandomPoint,2);
    wPrec = W;
    i = [-5, 15];
    cpt = 1;

    % Affichage graphique
    j = -(wPrec(1) + i * wPrec(2)) / wPrec(3) ;
    plot(i, j, 'k') ;
       
    % descente du gradient
    while(norm(wPrec - wCurrent) > epsilon)
       % Affichage graphique
       j = -(wCurrent(1) + i * wCurrent(2)) / wCurrent(3);
       plot(i, j, 'k') ;
       
       Ym = ChercherMauvaisPlacement(Y, wCurrent);
       if size(Ym,2) > 0
           index = randi(size(Ym,2));
           RandomPoint = [Ym(2,index) Ym(3,index)];
           wPrec  = wCurrent;
           wCurrent = wPrec + nu* sum(RandomPoint,2);
       else
           wPrec  = wCurrent;
           wCurrent = wPrec + nu* sum(Ym,2);
       end
       nu = nu/sqrt(cpt);
       cpt = cpt+1;
       pause(1);
    end
    % Affichage graphique
    j = -(wCurrent(1) + i * wCurrent(2)) / wCurrent(3) ;
    plot(i, j, 'k') ;
    W = wPrec;
end