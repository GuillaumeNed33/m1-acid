function [result] = MaxPosteriori2D(test, bar, saumon, proba_Bar, proba_Saumon)
    probBar= mvnpdf(test, bar.mu, bar.sigma);
    probSaumon = mvnpdf(test, saumon.mu, saumon.sigma);
    
    result = probBar*proba_Bar >= probSaumon*proba_Saumon;
end
