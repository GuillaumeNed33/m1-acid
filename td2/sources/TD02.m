% close all;
% 
% figure('Name', 'parabole et sa derivee') 
% hold on;
% 
% axis auto;
% X = -100: 100;
% Y = parabole(X);
% plot(X, Y)
% 
% 
% dY = diff(Y)./diff(X);
% length(X)
% length(dY)
% 
% % Comparer la courbe suivante :
% plot(X(:, 1:length(X)-1),dY,'r')
% 
% % Avec celle-ci
% Yd = 2*X;
% plot(X, Yd, 'g')
% 
% hold off
% 
% figure('Name', 'animation parabole')
% 
% % en un point de la parabole, on affiche la tangente (le coefficient directeur est la derivee)
% 
% hold on;
% 
% plot(X,Y)
% 
% for i=-80:20:80
%     
%     plot(i,parabole(i),'or');
%     
%     tg =2*i*10+parabole(i);
%     
%     plot([i;i+10],[parabole(i);tg],'r')
%     
%     pause(3);
% end

%% Exercice 1
% p(x) = x^2 + x + 3
% coeff = [3 1 1]
% puissance = [0 1 2]
% calcul en X [10 20 30]
coeff = [3 1 1];
X = 0:0.1:10;
v = valeurPolynome(coeff, X);

close all;
figure('Name', 'Valeur du polynome')
hold on;
plot(X, v)

coeffDeriv = derivPoly(coeff);
Y = valeurPolynome(coeffDeriv, X);
plot(X, Y)

hold off;

%% Exercice 2 -> voir fichier animationDescenteParabole.m

%% Exercice 3 
%poly = 30 - 61x + 41x² - 11x³ + x⁴
coeffs = [30 -61 41 -11 1];
x = -6:0.01:6;
y = valeurPolynome(coeffs, x);
coeffDeriv = derivPoly(coeffs);
Y = valeurPolynome(coeffDeriv, x);
Z = valeurPolynome(coeffDeriv, y);

close all;
figure('Name', 'recherche du minimum sur un polynome')
hold on;
plot(x,y, 'b');
plot(x,Y, 'g');
plot(x,Z, 'r');

epsilon = 0.001; % critere d'arret
x0 = 5;
xcurrent = x0 - 2*x0;
xprec = x0;
nu = 0.1;

plot(x0,parabole(x0),'ok','MarkerSize',20);
% descente du gradient
while(abs(xprec - xcurrent) > epsilon)
    plot(xcurrent,parabole(xcurrent),'ob');   
    xprec  = xcurrent;
    xcurrent = xprec - nu*2*xprec;
    pause(1);
end
plot(xcurrent,parabole(xcurrent),'xr', 'MarkerSize',20);


