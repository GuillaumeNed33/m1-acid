load('donnees1.mat');

%% Visualisation des echantillons 
figure('Name', 'Visualisations des echantillons')
hold on
scatter(C1(1, :), C1(2, :), C1(3, :));
scatter(C2(1, :), C2(2, :), C2(3, :));
hold off
close all;

%% Classifications
sizeTrain = 500;
nbIter = 100;
VectErrorC1_Max = [];
VectErrorC2_Max = [];
VectErrorC1_Perceptron = [];
VectErrorC2_Perceptron = [];
VectErrorC1_Carres = [];
VectErrorC2_Carres = [];
W = [6; -1];

for i=1:nbIter
    [TrainC1,TestC1,TrainC2, TestC2] = extractTestAndTrain(C1, C2, sizeTrain);
    
    %% Calcul ACP
    TrainACP = [TrainC1; TrainC2];
    CenterMean1 = mean(TrainACP(:, 1)) ;
    CenterMean2 = mean(TrainACP(:, 2));
    CenterMean3 = mean(TrainACP(:, 3));
    % Centrage de la matrice :
    TrainACP(:, 1) = TrainACP(:, 1) - CenterMean1;
    TrainACP(:, 2) = TrainACP(:, 2) - CenterMean2;
    TrainACP(:, 3) = TrainACP(:, 3) - CenterMean3;
    
    W_ACP = CalculACP(TrainACP);
    W_ACP = transpose(W_ACP(:,1));
    
    ProjTrainC1 = Projection(W_ACP, TrainC1);
    ProjTrainC2 = Projection(W_ACP, TrainC2);  
    ProjTestC1 = Projection(W_ACP, TestC1);
    ProjTestC2 = Projection(W_ACP, TestC2);
    
    %% Training
    [C1Train] = TrainModelMax(ProjTrainC1);
    [C2Train] = TrainModelMax(ProjTrainC2);
    [W_Perceptron] = TrainModelPerceptron(ProjTrainC1', ProjTrainC2', W);
    [W_Carres] = TrainModelCarres(ProjTrainC1', ProjTrainC2');
    
    %% Maximum de vraisemblance
    ResC1_Max = MaximumLikelihood(ProjTestC1, C1Train.mu, C1Train.sigma, C2Train.mu, C2Train.sigma);
    ResC2_Max = MaximumLikelihood(ProjTestC2, C1Train.mu, C1Train.sigma, C2Train.mu, C2Train.sigma);
   
    %% Perceptron
    ResC1_Perceptron = LinearClassify(ProjTestC1, W_Perceptron);
    ResC2_Perceptron = LinearClassify(ProjTestC2, W_Perceptron);
    
    %% Moindres carrés
    ResC1_Carres = LinearClassify(ProjTestC1, W_Carres);
    ResC2_Carres = LinearClassify(ProjTestC2, W_Carres);

    %% Récupération des erreurs
    [C1ErrorMax, C2ErrorMax ] = computeError(ResC1_Max, ResC2_Max);
    [C1ErrorPerceptron, C2ErrorPerceptron ] = computeError(ResC1_Perceptron', ResC2_Perceptron');
    [C1ErrorCarres, C2ErrorCarres ] = computeError(ResC1_Carres', ResC2_Carres');

    VectErrorC1_Max = [VectErrorC1_Max, C1ErrorMax];
    VectErrorC1_Perceptron = [VectErrorC1_Perceptron, C1ErrorPerceptron];
    VectErrorC1_Carres = [VectErrorC1_Carres, C1ErrorCarres];

    VectErrorC2_Max = [VectErrorC2_Max, C2ErrorMax];
    VectErrorC2_Perceptron = [VectErrorC2_Perceptron, C2ErrorPerceptron];
    VectErrorC2_Carres = [VectErrorC2_Carres, C2ErrorCarres];
end

%% Calcul des moyennes d'erreurs
moyErrorC1_Max = mean(VectErrorC1_Max)*100
moyErrorC2_Max = mean(VectErrorC2_Max)*100

moyErrorC1_Perceptron = mean(VectErrorC1_Perceptron)*100
moyErrorC2_Perceptron = mean(VectErrorC2_Perceptron)*100

moyErrorC1_Carres = mean(VectErrorC1_Carres)*100
moyErrorC2_Carres = mean(VectErrorC2_Carres)*100

%% Graphe des erreurs
figure('Name', 'Courbe des erreurs - MaxLikelihood');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;

figure('Name', 'Courbe des erreurs - Perceptron');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;

figure('Name', 'Courbe des erreurs - Moindres Carrés');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;