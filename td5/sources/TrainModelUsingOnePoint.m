function [W] = TrainModelUsingOnePoint (C1, C2)
    W = [6; 1; -3];
    %W = [-2.05;0.16; 0.17];
    epsilon = 0.01; % critere d'arret
    nu = 0.015;
    Y = CalculMatY(C1,C2);
    Ym = ChercherMauvaisPlacement(Y, W);
    index = randi(size(Ym,2));
    RandomPoint = [Ym(2,index) Ym(3,index)];
    wCurrent = W + nu * sum(RandomPoint,2);
    wPrec = W;
    i = [-5, 15];
    cpt = 1;
    % descente du gradient
    while(norm(wPrec - wCurrent) > epsilon)       
       Ym = ChercherMauvaisPlacement(Y, wCurrent);
       if size(Ym,2) > 0
           index = randi(size(Ym,2));
           RandomPoint = [Ym(2,index) Ym(3,index)];
           wPrec  = wCurrent;
           wCurrent = wPrec + nu* sum(RandomPoint,2);
       else
           wPrec  = wCurrent;
           wCurrent = wPrec + nu* sum(Ym,2);
       end
       nu = nu/sqrt(cpt);
       cpt = cpt+1;
    end
    W = wPrec;
end