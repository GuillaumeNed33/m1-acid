function [SaumonError, BarError ] = computeError(ResSaumon, ResBar )
    nbBarError    = size(ResBar,2) - sum(ResBar);
    sizeBar = size(ResBar,2);
    BarError = nbBarError / size(ResBar,2); %*100 pour le pourcentage

    nbSaumonError = sum(ResSaumon);
    sizeSaumon = size(ResSaumon,2);
    SaumonError = nbSaumonError / size(ResSaumon,2); %*100 pour le pourcentage
end
