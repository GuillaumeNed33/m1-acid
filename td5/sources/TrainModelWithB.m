function [nextW] = TrainModelWithB (C1, C2)
    epsilon = 0.01; % critere d'arret
    Y = CalculMatY(C1,C2);
    b = ones(size(Y, 2), 1) ;
    Z = Y';
    W = Z \ b;

    nextB = b - (-2 * ((Z * W) - b));
    currentB = b;
    nextW = W;
    
    %remplacement des négatifs par 0
    nextB(find(nextB < 0)) = 0 ;
    
    cpt = 1;      
    % descente du gradient
    while(norm(currentB - nextB) > epsilon)      
       currentB  = nextB;
       nextB = currentB - ((0.1/cpt) * (-2 * ((Z * W) - currentB)));
       nextB(find(nextB < 0)) = 0;
       
       cpt = cpt+1;
       nextW = Z \ nextB;
    end
end