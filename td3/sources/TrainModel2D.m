function [model] = TrainModel2D (Train)
  model.mu = mean(Train);
  model.sigma = cov(Train);
end