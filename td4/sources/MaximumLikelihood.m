function [result] = MaximumLikelihood(test, barMu, barSigma, saumonMu, saumonSigma)
    probBar= mvnpdf(test, barMu, barSigma);
    probSaumon = mvnpdf(test, saumonMu, saumonSigma);
    result = probBar >= probSaumon;
end
