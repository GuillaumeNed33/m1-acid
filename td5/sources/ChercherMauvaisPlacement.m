function [Ym] = ChercherMauvaisPlacement(Y, W)
    % du coté de C1, les valeurs W(1 i j) doivent être positives
    % du coté de C2, les valeurs W(1 i j) doivent être négatives
    index = find(W' * Y < 0);
    Ym = Y(:, index) ;
end