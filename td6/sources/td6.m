load('donnees.mat');

%% VISUALISATION DES ECHANTILLONS
figure('Name', 'Visualisation des echantillons');
hold on
scatter(trainC1(:, 1), trainC1(:, 2),'r');
scatter(trainC2(:, 1), trainC2(:, 2),'b');
scatter(testC2(:, 1), testC2(:, 2),'r', 'x');
scatter(testC2(:, 1), testC2(:, 2),'b','x');
hold off;
close all;

TRAIN = [trainC1;trainC2];
TEST = [testC1;testC2];

%% KNN
modelKNN = KDTreeSearcher(TRAIN);
[n, d] = knnsearch(modelKNN, TEST,'k',3);

%% SVM
modelSVM = fitcsvm(TRAIN, testC1, '', '', 'linear');
