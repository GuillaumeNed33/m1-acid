load('VTSaumonBar3.mat');
nbIter = 100;
sizeTrain = 100;
VectErrorBar = [];
VectErrorSaumon = [];
VectErrorBarProj = [];
VectErrorSaumonProj = [];

%% VISUALISATION DES ECHANTILLONS
figure('Name', 'Visualisation des echantillons');
scatter(VTSaumon(:, 1), VTSaumon(:, 2), VTSaumon(:, 3),'r');
hold on
scatter(VTBar(:, 1), VTBar(:, 2), VTBar(:, 3),'b');
close all;

figure('Name', 'ACI - 3D');

for i=1:nbIter
    %% Test and Train
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);

    %% Calcul ACI
    Projection = CalculACI(TrainBar, TrainSaumon);
    
    muProjBar = mean(Projection.X);
    muProjSaumon = mean(Projection.Y);
    
    SigProjBar = cov(Projection.X);
    SigProjSaumon = cov(Projection.Y);
    
    hold on
    scatter(TrainBar(:,1), TrainBar(:,2),'b');
    scatter(TrainSaumon(:,1), TrainSaumon(:,2),'o');
    plot([0 20],droite2DVd([0 20],Projection.X, [0 0]),'r');
    plot([0 20],droite2DVd([0 20],Projection.Y, [0 0]),'r');

    %% Utilisation du classifieur
    ResBar = MaximumLikelihood(TestBar, BarTrain.mu, BarTrain.sigma, SaumonTrain.mu, SaumonTrain.sigma);
    ResSaumon = MaximumLikelihood(TestSaumon, BarTrain.mu, BarTrain.sigma, SaumonTrain.mu, SaumonTrain.sigma);
    
    ResBarProj = MaximumLikelihood(Projection.X,muProjBar,SigProjBar,muProjSaumon,SigProjSaumon);
    ResSaumonProj = MaximumLikelihood(Projection.Y,muProjBar,SigProjBar,muProjSaumon,SigProjSaumon);
    
    %% Récupération des erreurs
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
    
    [SaumonErrorProj, BarErrorProj ] = computeError(ResSaumonProj, ResBarProj);
    VectErrorBarProj = [VectErrorBarProj, BarErrorProj];
    VectErrorSaumonProj = [VectErrorSaumonProj, SaumonErrorProj];
end;
hold off

%% Calcul des moyennes d'erreurs
moyErrorBar = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumon = mean(VectErrorSaumon)*100 % idem pour saumon

moyErrorBarProj = mean(VectErrorBarProj)*100 
moyErrorSaumonProj = mean(VectErrorSaumonProj)*100

%% Graphe des erreurs
%figure('Name', 'Courbe des erreurs - Max Likelihood');
%hold on;
%plot((1:nbIter), VectErrorBar, 'b', (1:nbIter), VectErrorSaumon, 'r')
%hold off;
%close all;