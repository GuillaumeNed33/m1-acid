function [ coeffDeriv ] = derivPoly( coeff )
puissance = 0:length(coeff)-1;
coeffDeriv = puissance .* coeff;
coeffDeriv(1)= [];
end

