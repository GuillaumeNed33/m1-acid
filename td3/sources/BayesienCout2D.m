function [result] = BayesienCout2D(test, bar, saumon, tabCout)
    CoutSaumonCommeBar = tabCout(1,2); %lambda bs
    CoutBarCommeSaumon = tabCout(2,1); %lambda sb
    
    prob1= mvnpdf(test, bar.mu, bar.sigma);
    prob2 = mvnpdf(test, saumon.mu, saumon.sigma);
    
    RiskSaumon = CoutBarCommeSaumon * prob1;
    RiskBar = CoutSaumonCommeBar * prob2;

    result = RiskBar <= RiskSaumon;
end