function [result] = MaxPosteriori(test, bar, saumon, proba_Bar, proba_Saumon)
    probBar= normpdf(test, bar.mu, bar.sigma);
    probSaumon = normpdf(test, saumon.mu, saumon.sigma);
    
    result = probBar*proba_Bar >= probSaumon*proba_Saumon;
end
