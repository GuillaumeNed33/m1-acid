load('VTSaumonBar.mat');

%% visualisation des  données
figure('Name', 'Répartition des données');
hold on;
scatter(VTSaumon(:, 1), VTSaumon(:,1));
scatter(VTBar(:, 1), VTBar(:,1));
hold off;

%% Vérité terrain
sizeVTSaumon = size(VTSaumon,1);
sizeVTBar = size(VTBar, 1);
probBar = sizeVTBar / (sizeVTBar + sizeVTSaumon);
probSaumon = sizeVTSaumon / (sizeVTBar + sizeVTSaumon);
tabCout = [ 0 2; 1 0];
nbIter = 100;
sizeTrain = 100;

hold on ;
figure('Name', 'histogramme Saumon');
histogram(VTSaumon) ; 

figure('Name', 'histogramme Bar');
histogram(VTBar);
hold off ;

%close all ;

%% Classifieur classique
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);
    
    ResBar = Myclassify(TestBar, BarTrain, SaumonTrain);
    ResSaumon = Myclassify(TestSaumon, BarTrain, SaumonTrain);
   
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarEx1 = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonEx1 = mean(VectErrorSaumon)*100 % idem pour saumon

hold on;
figure('Name', 'Courbe des erreurs - Maximum de Vraisemblance');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;

%close all;

%% Classifieur Max a Posteriori
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter   
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain);
    
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);
    
    ResBar = MaxPosteriori(TestBar, BarTrain, SaumonTrain, probBar, probSaumon);
    ResSaumon = MaxPosteriori(TestSaumon, BarTrain, SaumonTrain, probBar, probSaumon);

    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarMaxPosteriori = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonMaxPosteriori = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Max Posteriori');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;

% close all;

%% Classifieur Bayesien Cout
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
    [TrainSaumon, TestSaumon, TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain) ;
   
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);
    
    ResBar = BayesienCout(TestBar, BarTrain, SaumonTrain, tabCout);
    ResSaumon = BayesienCout(TestSaumon, BarTrain, SaumonTrain, tabCout);
    
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarCout = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonCout = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Cout');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;
% close all;

%% Classifieur Bayesien Cout + MAP
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
    [TrainSaumon, TestSaumon, TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain) ;
    
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);
    
    ResBar = BayesienCoutWithMaxPosteriori(TestBar,BarTrain,SaumonTrain, tabCout, probBar, probSaumon);
    ResSaumon = BayesienCoutWithMaxPosteriori(TestSaumon,BarTrain,SaumonTrain,tabCout, probBar, probSaumon);
    
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarCoutEtMax = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonCoutEtMax = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Cout avec MAX POSTERIORI');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;
%close all;
