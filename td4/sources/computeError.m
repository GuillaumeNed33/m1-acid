function [SaumonError, BarError ] = computeError(ResSaumon, ResBar)
    nbBarError    = size(ResBar,1) - sum(ResBar);
    BarError = nbBarError / size(ResBar,1); %*100 pour le pourcentage

    nbSaumonError = sum(ResSaumon);
    SaumonError = nbSaumonError / size(ResSaumon,1); %*100 pour le pourcentage
end
