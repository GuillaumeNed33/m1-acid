%% Classifieur  pour descripteur de taille différente
load('VTSaumonBar2.mat')
%load('VTAlzheimer2.mat')

%VTSaumon = VTCN;
%VTBar = VTAD;
%% visualisation des  données
figure('Name', 'Répartition');
hold on;
scatter(VTSaumon(:, 1), VTSaumon(:,2));
scatter(VTBar(:, 1), VTBar(:,2));
hold off;

%% Vérité terrain
 sizeVTSaumon = size(VTSaumon,1);
 sizeVTBar = size(VTBar, 1);
 probBar = sizeVTBar / (sizeVTBar + sizeVTSaumon);
 probSaumon = sizeVTSaumon / (sizeVTBar + sizeVTSaumon);
 tabCout = [ 0 2; 1 0];
 nbIter = 100;
 sizeTrain = 100;
%close all ;

%% Classifieur classique
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain2D(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel2D(TrainSaumon);
    [BarTrain] = TrainModel2D(TrainBar);
    
    ResBar = Myclassify2D(TestBar, BarTrain, SaumonTrain);
    ResSaumon = Myclassify2D(TestSaumon, BarTrain, SaumonTrain);
   
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarEx1 = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonEx1 = mean(VectErrorSaumon)*100 % idem pour saumon

hold on;
figure('Name', 'Courbe des erreurs - Maximum de vraisemblance');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;

%close all;

%% Classifieur Max a Posteriori
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter   
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain2D(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel2D(TrainSaumon);
    [BarTrain] = TrainModel2D(TrainBar);
    
    ResBar = MaxPosteriori2D(TestBar, BarTrain, SaumonTrain, probBar, probSaumon);
    ResSaumon = MaxPosteriori2D(TestSaumon, BarTrain, SaumonTrain, probBar, probSaumon);

    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarMaxPosteriori = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonMaxPosteriori = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Max Posteriori');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;

% close all;

%% Classifieur Bayesien Cout
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain2D(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel2D(TrainSaumon);
    [BarTrain] = TrainModel2D(TrainBar);
    
    ResBar = BayesienCout2D(TestBar, BarTrain, SaumonTrain, tabCout);
    ResSaumon = BayesienCout2D(TestSaumon, BarTrain, SaumonTrain, tabCout);
    
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarCout = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonCout = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Cout');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;
% close all;

%% Classifieur Bayesien Cout + MAP
VectErrorBar = [];
VectErrorSaumon = [];
for i=1:nbIter
   [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain2D(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel2D(TrainSaumon);
    [BarTrain] = TrainModel2D(TrainBar);
    
    ResBar = BayesienCoutWithMaxPosteriori2D(TestBar,BarTrain,SaumonTrain, tabCout, probBar, probSaumon);
    ResSaumon = BayesienCoutWithMaxPosteriori2D(TestSaumon,BarTrain,SaumonTrain,tabCout, probBar, probSaumon);
    
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
end;
moyErrorBarCoutEtMax = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumonCoutEtMax = mean(VectErrorSaumon)*100 % idem pour saumon


hold on;
figure('Name', 'Courbe des erreurs - Cout avec MAX POSTERIORI');
plot([1:nbIter], VectErrorBar, 'b', [1:nbIter], VectErrorSaumon, 'r')
hold off;
%close all;