function [nextW] = PerceptronUsingB(C1, C2, W, b)
    epsilon = 0.01; % critere d'arret
    Y = CalculMatY(C1,C2);
    Z = Y';
    nextB = b - (-2 * ((Z * W) - b));
    currentB = b;
    
    currentW = W;
    nextW = (2 * Z') * ((Z * W) - b);
    
    %remplacement des négatifs par 0
    nextB(find(nextB < 0)) = 0 ;
    
    i = [-5, 15];
    cpt = 1;

    % Affichage graphique
    j = -(W(1) + i * W(2)) / W(3) ;
    plot(i, j, 'k') ;
       
    % descente du gradient
    while(norm(currentB - nextB) > epsilon)
       % Affichage graphique
       j = -(nextW(1) + i * nextW(2)) / nextW(3);
       plot(i, j, 'k') ;
       
       currentB  = nextB;
       nextB = currentB - ((0.1/cpt) * (-2 * ((Z * W) - currentB)));
       nextB(find(nextB < 0)) = 0;
       
       cpt = cpt+1;
       nextW = Z \ nextB;
       pause(1);
    end
    % Affichage graphique
    j = -(nextW(1) + i * nextW(2)) / nextW(3) ;
    plot(i, j, 'k') ;
end