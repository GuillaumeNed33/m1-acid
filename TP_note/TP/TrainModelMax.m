function [model] = TrainModelMax (TrainC1)
  model.mu = mean(TrainC1);
  model.sigma = cov(TrainC1);
end
