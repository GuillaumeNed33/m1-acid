function [result] = Myclassify(test, bar, saumon)
    probBar= normpdf(test, bar.mu, bar.sigma);
    probSaumon = normpdf(test, saumon.mu, saumon.sigma);
    result = probBar >= probSaumon;
end
