function [Y] = CalculMatY(C1, C2)
    l1 = [ones(1,size(C1, 2)) ; C1] ;
    l2 = [ones(1, size(C2, 2)) ; C2] ;
    Y = [l1 -l2] ;
end