function [result] = BayesienCout(test, bar, saumon, tabCout)
    CoutSaumonCommeBar = tabCout(1,2); %lambda bs
    CoutBarCommeSaumon = tabCout(2,1); %lambda sb
    
    prob1= normpdf(test, bar.mu, bar.sigma);
    prob2 = normpdf(test, saumon.mu, saumon.sigma);
    
    RiskSaumon = CoutBarCommeSaumon * prob1;
    RiskBar = CoutSaumonCommeBar * prob2;

    result = RiskBar <= RiskSaumon;
end