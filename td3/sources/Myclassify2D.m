function [result] = Myclassify2D(test, bar, saumon)
    probBar= mvnpdf(test, bar.mu, bar.sigma);
    probSaumon = mvnpdf(test, saumon.mu, saumon.sigma);
    result = probBar >= probSaumon;
end
