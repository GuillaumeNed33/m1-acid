load('donnees1.mat');

%% Visualisation des echantillons 
figure('Name', 'Visualisations des echantillons')
hold on
scatter(C1(1, :), C1(2, :), C1(3, :));
scatter(C2(1, :), C2(2, :), C2(3, :));
hold off
close all;

%% Analyse des données
% meanC1_D1 = mean(C1(1, :));
% meanC1_D2 = mean(C1(2, :));
% meanC1_D3 = mean(C1(3, :));
% meanC2_D1 = mean(C2(1, :));
% meanC2_D2 = mean(C2(2, :));
% meanC2_D3 = mean(C2(3, :));
% 
% minC1_D1 = min(C1(1, :));
% minC1_D2 = min(C1(2, :));
% minC1_D3 = min(C1(3, :));
% minC2_D1 = min(C2(1, :));
% minC2_D2 = min(C2(2, :));
% minC2_D3 = min(C2(3, :));
% 
% maxC1_D1 = max(C1(1, :));
% maxC1_D2 = max(C1(2, :));
% maxC1_D3 = max(C1(3, :));
% maxC2_D1 = max(C2(1, :));
% maxC2_D2 = max(C2(2, :));
% maxC2_D3 = max(C2(3, :));

%% Classifications
sizeTrain = 500;
nbIter = 100;
VectErrorC1_Max = [];
VectErrorC2_Max = [];
VectErrorC1_Perceptron = [];
VectErrorC2_Perceptron = [];
VectErrorC1_Carres = [];
VectErrorC2_Carres = [];

%% 3 descripteurs
W = [6; -1; 2; 3];

%% 2 descripteurs (1 et 2)
% C1(:, 3) = [];
% C2(:, 3) = [];
% W = [6; -1; 2];

%% 2 descripteurs (1 et 3)
% C1(:, 2) = [];
% C2(:, 2) = [];
% W = [6; -1; 2];

for i=1:nbIter
    [TrainC1,TestC1,TrainC2, TestC2] = extractTestAndTrain(C1, C2, sizeTrain);
    [C1Train] = TrainModelMax(TrainC1);
    [C2Train] = TrainModelMax(TrainC2);
    [W_Perceptron] = TrainModelPerceptron(TrainC1', TrainC2', W);
    [W_Carres] = TrainModelCarres(TrainC1', TrainC2');
    
    %% Maximum de vraisemblance
    ResC1_Max = MaximumLikelihood(TestC1, C1Train.mu, C1Train.sigma, C2Train.mu, C2Train.sigma);
    ResC2_Max = MaximumLikelihood(TestC2, C1Train.mu, C1Train.sigma, C2Train.mu, C2Train.sigma);
   
    %% Perceptron
    ResC1_Perceptron = LinearClassify(TestC1, W_Perceptron);
    ResC2_Perceptron = LinearClassify(TestC2, W_Perceptron);
    
    %% Moindres carrés
    ResC1_Carres = LinearClassify(TestC1, W_Carres);
    ResC2_Carres = LinearClassify(TestC2, W_Carres);

    %% Récupération des erreurs
    [C1ErrorMax, C2ErrorMax ] = computeError(ResC1_Max, ResC2_Max);
    [C1ErrorPerceptron, C2ErrorPerceptron ] = computeError(ResC1_Perceptron', ResC2_Perceptron');
    [C1ErrorCarres, C2ErrorCarres ] = computeError(ResC1_Carres', ResC2_Carres');

    VectErrorC1_Max = [VectErrorC1_Max, C1ErrorMax];
    VectErrorC1_Perceptron = [VectErrorC1_Perceptron, C1ErrorPerceptron];
    VectErrorC1_Carres = [VectErrorC1_Carres, C1ErrorCarres];

    VectErrorC2_Max = [VectErrorC2_Max, C2ErrorMax];
    VectErrorC2_Perceptron = [VectErrorC2_Perceptron, C2ErrorPerceptron];
    VectErrorC2_Carres = [VectErrorC2_Carres, C2ErrorCarres];
end

%% Calcul des moyennes d'erreurs
moyErrorC1_Max = mean(VectErrorC1_Max)*100
moyErrorC2_Max = mean(VectErrorC2_Max)*100

moyErrorC1_Perceptron = mean(VectErrorC1_Perceptron)*100
moyErrorC2_Perceptron = mean(VectErrorC2_Perceptron)*100

moyErrorC1_Carres = mean(VectErrorC1_Carres)*100
moyErrorC2_Carres = mean(VectErrorC2_Carres)*100

%% Graphe des erreurs
figure('Name', 'Courbe des erreurs - MaxLikelihood');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;

figure('Name', 'Courbe des erreurs - Perceptron');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;

figure('Name', 'Courbe des erreurs - Moindres Carrés');
hold on;
plot((1:nbIter), VectErrorC1_Max, 'b', (1:nbIter), VectErrorC2_Max, 'r')
hold off;
%close all;