function [ TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain2D(VTSaumon, VTBar, sizeTrain)
    sizeVTSaumon = size(VTSaumon,1);
    sizeVTBar = size(VTBar, 1);

    TrainSaumonIndice = randperm(sizeVTSaumon, sizeTrain);
    TrainBarIndice = randperm(sizeVTBar, sizeTrain);

    TrainBar = VTBar(TrainBarIndice, :);
    TrainSaumon = VTSaumon(TrainSaumonIndice, :);
    
    TestBar = VTBar;
    TestSaumon = VTSaumon;
    TestSaumon(TrainSaumonIndice, :) = [] ;
    TestBar(TrainBarIndice, :) = [] ;
end

