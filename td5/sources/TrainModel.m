function [W] = TrainModel (C1, C2)    
    W = [6; 1; -3];
    %W = [-2.05;0.16; 0.17];
    epsilon = 0.005; % critere d'arret
    nu = 0.015;
    Y = CalculMatY(C1,C2);
    Ym = ChercherMauvaisPlacement(Y, W);
    wCurrent = W + nu * sum(Ym,2);
    wPrec = W;
    i = [-5, 15] ;
    cpt = 1;
    % descente du gradient
    while(norm(wPrec - wCurrent) > epsilon)
       Ym = ChercherMauvaisPlacement(Y, wCurrent);
       wPrec  = wCurrent;
       wCurrent = wPrec + nu* sum(Ym,2);  
       nu = nu/sqrt(cpt);   
       cpt = cpt+1;
    end
    W = wPrec;
end