load('VTSaumonBar2.mat');
nbIter = 100;
sizeTrain = 100;
VectErrorBar = [];
VectErrorSaumon = [];
VectErrorBarProj = [];
VectErrorSaumonProj = [];

%% VISUALISATION DES ECHANTILLONS
figure('Name', 'Visualisation des echantillons');
scatter(VTSaumon(:, 1), VTSaumon(:, 2),'r');
hold on
scatter(VTBar(:, 1), VTBar(:, 2),'b');
close all;

figure('Name', 'ACI');

for i=1:nbIter
    %% Test and Train
    [TrainSaumon,TestSaumon,TrainBar, TestBar] = extractTestAndTrain(VTSaumon, VTBar, sizeTrain);
    [SaumonTrain] = TrainModel(TrainSaumon);
    [BarTrain] = TrainModel(TrainBar);

    %% Calcul ACI
    TrainACI = CalculACI(TrainBar, TrainSaumon);
    
    %% Projection  
    ProjTrainBar = Projection(transpose(TrainACI(:,1)), TrainBar);
    ProjTrainSaumon = Projection(transpose(TrainACI(:,1)), TrainSaumon);  
    ProjTestBar = Projection(transpose(TrainACI(:,1)), TestBar);
    ProjTestSaumon = Projection(transpose(TrainACI(:,1)), TestSaumon);
    
    muProjBar = mean(ProjTrainBar);
    muProjSaumon = mean(ProjTrainSaumon);
    
    SigProjBar = cov(ProjTrainBar);
    SigProjSaumon = cov(ProjTrainSaumon);
    
    hold on
    scatter(TrainBar(:,1), TrainBar(:,2),'b');
    scatter(TrainSaumon(:,1), TrainSaumon(:,2),'o');
    plot([0 20],droite2DVd([0 20],ProjTestBar, [0 0]),'r');
    plot([0 20],droite2DVd([0 20],ProjTestSaumon, [0 0]),'r');

    %% Utilisation du classifieur
    ResBar = MaximumLikelihood(TestBar, BarTrain.mu, BarTrain.sigma, SaumonTrain.mu, SaumonTrain.sigma);
    ResSaumon = MaximumLikelihood(TestSaumon, BarTrain.mu, BarTrain.sigma, SaumonTrain.mu, SaumonTrain.sigma);
    
    ResBarProj = MaximumLikelihood(ProjTestBar,muProjBar,SigProjBar,muProjSaumon,SigProjSaumon);
    ResSaumonProj = MaximumLikelihood(ProjTestSaumon,muProjBar,SigProjBar,muProjSaumon,SigProjSaumon);
    
    %% Récupération des erreurs
    [SaumonError, BarError ] = computeError(ResSaumon, ResBar);
    VectErrorBar = [VectErrorBar, BarError];
    VectErrorSaumon = [VectErrorSaumon, SaumonError];
    
    [SaumonErrorProj, BarErrorProj ] = computeError(ResSaumonProj, ResBarProj);
    VectErrorBarProj = [VectErrorBarProj, BarErrorProj];
    VectErrorSaumonProj = [VectErrorSaumonProj, SaumonErrorProj];
end;
hold off

%% Calcul des moyennes d'erreurs
moyErrorBar = mean(VectErrorBar)*100 % pourcentage d'erreur bar
moyErrorSaumon = mean(VectErrorSaumon)*100 % idem pour saumon

moyErrorBarProj = mean(VectErrorBarProj)*100 
moyErrorSaumonProj = mean(VectErrorSaumonProj)*100

%% Graphe des erreurs
%figure('Name', 'Courbe des erreurs - Max Likelihood');
%hold on;
%plot((1:nbIter), VectErrorBar, 'b', (1:nbIter), VectErrorSaumon, 'r')
%hold off;
%close all;