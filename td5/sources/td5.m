load('donnees.mat');
sizeTrain = 300;
nbIter = 100;
VectErrorC1 = [];
VectErrorC2 = [];
VectErrorC3 = [];
VectErrorC4 = [];

%% Ensemble séparable
%C1 = [2 3 4; 1 2 2];
%C1 = C1'
%C2 = [1 1 2 5; 2 3 3 3];
%C2 = C2'
%C3 = [5 0 ; 3 4];
%C3 = C3'
%C4 = [5 6 ; 1 3];
%C4 = C4'

%% Ensemble non séparable
%C1 = [2 3 4; 1 3 2];
%C1 = C1'
%C2 = [1 1 2 5; 2 3 3 3];
%C2 = C2'

%% Visualisation des echantillons 
figure('Name', 'Visualisations des echantillons')
hold on
axis equal
scatter(C1(:, 1), C1(:, 2),'r');
scatter(C2(:, 1), C2(:, 2),'b');
%scatter(C3(:, 1), C3(:, 2),'g');
%scatter(C4(:, 1), C4(:, 2:),'o');

%% PARTIE PERCEPTRON
%% Test and Train
[TrainC1,TestC1,TrainC2, TestC2] = extractTestAndTrain(C1, C2, sizeTrain);
%[TrainC3,TestC3,TrainC4, TestC4] = extractTestAndTrain(C3, C4, sizeTrain);

%% Calcul de la matrice Y et Z, calcul de b
Y = CalculMatY(TrainC1',TrainC2');
%Y2 = CalculMatY(TrainC3',TrainC4');
Z = Y';
%Z2 = Y2';
b = ones(size(Y, 2), 1) ;
%b2 = ones(size(Y2, 2), 1) ;

%% Pour l'exercice 3; commentez :
%W = [6; 1; -3];
%decommentez :
W = Z \ b;
%W2 = Z2 \b2;

%% Points mal placé initalement
YmBefore = ChercherMauvaisPlacement(Y, W);
%YmBefore2 = ChercherMauvaisPlacement(Y2, W2);

%% Descente de gradient 
%W = Perceptron(TrainC1',TrainC2', W);
%W = PerceptronUsingOnlyOnePoint(TrainC1',TrainC2', W);
%W = PerceptronUsingB(TrainC1', TrainC2', W, b);
%W2 = PerceptronUsingB(TrainC3', TrainC4', W2, b2);

%% Points mal placés après la descente
YmAfter = ChercherMauvaisPlacement(Y, W);
%YmAfter2 = ChercherMauvaisPlacement(Y2, W2);


%% PARTIE CLASSIFICATION
for i=1:nbIter
    [TrainC1,TestC1,TrainC2, TestC2] = extractTestAndTrain(C1, C2, sizeTrain);
    %[W_Classify] = TrainModel(TrainC1', TrainC2');
    %[W_Classify] = TrainModelUsingOnePoint(TrainC1', TrainC2');
    [W_Classify] = TrainModelWithB(TrainC1', TrainC2');

    ResC1 = LinearClassify(TestC1, W_Classify);
    ResC2 = LinearClassify(TestC2, W_Classify);
    %ResC3 = LinearClassify(TestC3, W_Classify);
    %ResC4 = LinearClassify(TestC4, W_Classify);

    %% Récupération des erreurs
    [C1Error, C2Error ] = computeError(ResC2, ResC1);
    %[C3Error, C4Error ] = computeError(ResC3, ResC4);
    VectErrorC1 = [VectErrorC1, C1Error];
    VectErrorC2 = [VectErrorC2, C2Error];
    % VectErrorC3 = [VectErrorC3, C3Error];
    % VectErrorC4 = [VectErrorC4, C4Error];
end
%% Calcul des moyennes d'erreurs
moyErrorC1 = mean(VectErrorC1)*100 
moyErrorC2 = mean(VectErrorC2)*100 
% moyErrorC3 = mean(VectErrorC3)*100 
% moyErrorC4 = mean(VectErrorC4)*100 
close all