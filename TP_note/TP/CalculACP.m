function [V] = CalculACP(Echantillon)
    %Calcul de la moyenne des echantillons 
    moy = mean(Echantillon); 

    %Recentre les échantillons par rapport à la moyenne
    C = Echantillon - repmat(moy,size(Echantillon, 1), 1); 

    n = size(C,1)-1;
    [V, D]= eig(n.*cov(C));

    [arraySorted indexSorted] = sort(diag(D),'descend') ;
    V = V(:, indexSorted);
end

