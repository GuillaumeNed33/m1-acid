function [ v ] = valeurPolynome( coeff, x )
l = length(coeff);
puissance = 0:l-1;
tx = repmat(x,l,1);
v = coeff * (tx .^ (puissance.'));
end

