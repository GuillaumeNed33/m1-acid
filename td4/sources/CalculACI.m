function [V] = CalculACI(EchantillonC1, EchantillonC2)
    muEchantillonC1 = mean(EchantillonC1);
    muEchantillonC2 = mean(EchantillonC2);

    sigEchantillonC1 = cov(EchantillonC1);
    sigEchantillonC2 = cov(EchantillonC2);

    Sw = sigEchantillonC1 + sigEchantillonC2 ;
    Sb = (muEchantillonC1-muEchantillonC2)*(muEchantillonC1-muEchantillonC2)';

    invSw = inv(Sw); 
    invSwSb = invSw*Sb; 

    [V d] = eig(invSwSb);
end
 